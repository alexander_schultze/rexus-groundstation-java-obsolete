SDCI Readme
-----------
This program required the following tools to run
Netbeans IDE
Java Development Kit (JDK)
RXTX Library Files (rxtxserial.dll in /system32)

It requires the following Librarys to link
RXTX Library
JFreeGraph
-----------

This Software is under a CC-License. You are free to Share and Remix this Software.
http://creativecommons.org/licenses/by-nc-sa/3.0/