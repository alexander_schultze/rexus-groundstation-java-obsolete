/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		22.07.2013
 *
 *
 * ------------------------------------------------------------- */
package Main;

import BaseClasses.DataHandler;
import BaseClasses.DynamicChart;
import BaseClasses.SerialPort_Connector;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.swing.JLabel;

/**
 *
 * @author Alex
 */
public class Main {
    public static  GUI window;
    public static DataHandler experiment, adc1a,adc1b,adc2,temp, log, raw_comm;
  
    public static DynamicChart chart_adc1a,chart_adc1b,chart_adc2,chart_temp,chart_experiment;
   
   
    public static SerialPort_Connector com_port;
    public static Status status;
    public static Sensor[] sensors = new Sensor[3];
    public static Heating[] heating = new Heating[3];
    public static Pressure[] pressure = new Pressure[2];
   
    /* ----------------Struct for STATUS ------------------------*/
   public static class Status {
   public int Time_LO=0;
   public int Time_SYS=0;
   public int Hatch=0;
   public int Status=0;
  
   private int _last_crc_err=0;    
   private int _last_crc_ok=0;  
   /* Power and Voltages */
   public int u16_u_bat;
   public int u16_i_bat;
   public int u16_i_rexus;
   
   public double U_bat;
   public double I_bat;
   public double I_rexus;
   public double P_bat;
   public double P_rexus;
   
   public void update(){
     this.U_bat=(u16_u_bat/Math.pow(2,12)*3.3);   
     this.I_bat=(u16_i_bat/Math.pow(2,12)*3.3);
     this.I_rexus=(u16_i_bat/Math.pow(2,12)*3.3);
     
     this.P_bat=U_bat*I_bat;
     this.P_rexus=I_rexus * 28;
     
   }
   
   public boolean getComStatus(){
       boolean result;
       /* Have there been Packages Received since last time ? */
       result =_last_crc_ok>SerialPort_Connector.stat_rx_packages;
         /* Was the CRC Error Rate less than 30% ? */
       result&= ((float)SerialPort_Connector.stat_crc_err-_last_crc_err)/(SerialPort_Connector.stat_rx_packages-_last_crc_ok)<0.3;
       
     _last_crc_err= SerialPort_Connector.stat_crc_err;
     _last_crc_ok= SerialPort_Connector.stat_rx_packages;
 
    return result;   
   }
   
      public boolean getLOStatus(){
    return (Time_LO>0);   
   }
      
     public int getLOTime(){
    return Time_LO;   
   }
      public int getSysTime(){
    return Time_SYS;   
   }
      
     public int getStatus(){
    return Status;   
   }
      
    public int getHatch(){
    return Hatch;   
   }  
   }
    /* ----------------Struct for Implementations ------------------------*/
   public class Sensor {
   public int u16_u;
   public int u16_i;
   public int gain;
   
   double U;
   double I;
   double R;
     public void update(){
     this.U=(u16_u/Math.pow(2,12)*3.3);
     I=u16_i/Math.pow(2,12)*3.3*0.001*gain;  
     R=U/I;
     }
   }
   
      public class Heating{
    /* Structural Parameters */
   public double R0=1;
   
   public int u16_u=0;
   public int u16_i=0;
  
   
   double U=0;
   double I=0;
   double R=0;
   double P=0;
   double T=0;
   public void update(){
       U=u16_u/Math.pow(2,12)*3.3;
       I=u16_i/Math.pow(2,12)*3.3*0.001;
       R=U/I;
       P=U*I;
       T=PT1000_calcTemp(R0, R);
   }
   
   
   }
      
         public class Pressure {
   public int u16_u;
  /*Spannungsteiler */
   double div = 3.0 / (2.2+3);
   double U;
   
   
   double P; /* mBar */
   
   public void getPressureVSP62(){
     
   U=((u16_u/Math.pow(2,12)*3.3))*div;
   P=Math.pow(10,U-5.5);
   }
   
     public void getPressureJUMO(){
     /* Annahme ist Linearer Messbereich zwischen 1-5 Volt, Abbildung auf 0-1 Bar. */
   U=((u16_u/Math.pow(2,12)*3.3))*div;
  
   P=(U-1)*1000/4;
   }
   
   }
    
    
  public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
      
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

    
  /*java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
               new GUI().setVisible(true);
               
               
            }
        });  */
        
        window =new GUI();
     
     /* Set the Locale for the Right View of Numbers. Does not work now though, its all US */
      Locale.setDefault(Locale.GERMAN);
        
        /* Start Timer Threads */
       Runnable rs = new Timer();
      Thread s = new Thread(rs,"Timer1s");
      s.start();  
      
      /* Create Log Directory */
       new File("logs").mkdir();
       
       
      /* I like to move it move it */
       status = new Status();
       
       
       
      /* Connect all the necessary Data Handlers to Outputs */
      
      chart_adc1a = new DynamicChart("ADC Values (30s)",30*1000);
      chart_temp = new DynamicChart("Temp Values (30s)",30*1000);  
      chart_adc1b= new DynamicChart("ADC Values",30000);
      chart_adc2 = new DynamicChart("Temp Values",30000);
      chart_experiment = new DynamicChart("Temp Values",30000);
      
      adc1a = new DataHandler(new String[]{"ADC0","ADC1","ADC2","ADC3","ADC4","ADC5","ADC6"},chart_adc1a,"logs/adc1a_values_",window.jLog3);
      adc1b = new DataHandler(new String[]{"ADC7","ADC8","ADC9","ADC10","ADC11","ADC12","ADC13"},chart_adc1b,"logs/adc1b_values_",window.jLog3);
      adc2 = new DataHandler(new String[]{"ADC2_0","ADC2_1","ADC2_2","ADC2_3"},chart_adc2,"logs/adc2_values_",window.jLog3);
      temp = new DataHandler(new String[]{"STM32","T1","T2","T3"},chart_temp,"logs/temp_values_",window.jLog4);
      experiment = new DataHandler(new String[]{"Pressure 1","Pressure 2","Hatch 1","Sensor 1 - U","Sensor 1 - I","Sensor 2 - U","Sensor 2 - I","Sensor 3 - U","Sensor 3 - I","Heating 1 - R","Heating 2 - R","Heating 3 - R"},chart_experiment,"logs/experiment_",null);
      
    //  DataHandler graph1=new DataHandler(new String[]{"ADC1","ADC2","ADC3","ADC4"},chart_adc,null,null);
    //  DataHandler graph2=new DataHandler(new String[]{"STM32","T1","T2","T3"},chart_temp,null,null);
      
      /* Create Additional Data Handler for the Logging */
      log=new DataHandler("logs/log_",window.log);
      raw_comm=new DataHandler("logs/raw_",window.jLog2);
      
     
      
    //  window.jPanel18.setLayout(new java.awt.BorderLayout());
      
      /* Connect the Dynamic Charts to a Panel to be drawn to */
         chart_adc1a.createChartWindowed("Temperature", 30000);
   //  window.jPanel18.add(chart_adc_30s);
    JLabel label =new JLabel();
    label.setText("olla");
    


    
    //chart_adc1a.addObservation(0.1, 0);
   // chart_adc1a.addObservation(0.2, 0);
    chart_adc1a.setSize(window.ChartPanelP.getSize());
    window.ChartPressure.add(chart_adc1a);
    
     window.ChartPanelP.add(chart_temp);
     window.update();
    window.setVisible(true);
    
    
    // window.jChartPanel4.add(chart_temp);

     
     SerialPort_Connector.addDataHandler(adc1a, temp, log, raw_comm);
    
            try {
        
          log.addString("-----------------------------------------------\n");
          log.addString(getDateTime()+"\n");
          log.addString("Host: "+InetAddress.getLocalHost().getHostName()+"\n");
          log.addString("User: "+System.getProperty("user.name")+"\n");
          log.addString("-----------------------------------------------\n");
             
       
        } catch (UnknownHostException ex) {
           log.addString("(EXCEPTION)"+ex.toString());
        }
        window.getAvailableSerialPorts();
            
         /* Create and display the form */
  
  
    
             
     
    }
      
        
        
      public double PT1000_calcTemp(double R0, double R){
      /* According to http://oliverbetz.de/pt100/pt100.htm */
          double r=(R/R0)-1;
          return  (r * (255.8723 + r * (9.6 + r * 0.878)));
         
      }
  
      public static void update_chartsizes(){
    if(chart_adc1a!=null){
      chart_adc1a.setSize(window.ChartPanelI.getSize());
      chart_adc1b.setSize(window.ChartPanelI.getSize()); 
      chart_temp.setSize(window.ChartPressure.getSize());
    //  chart_temp.setSize(window.jChartPanel4.getSize());
    }
    }
    
    
      public static String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy @ HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }
      
          public static void jLog_Startline(){
     window.log.append("\n"+getTime()+">");
     window.log.setCaretPosition( window.log.getDocument().getLength() );
    
        
    }
          
      public static void updateGUI(){
          
          window.update();
      }
    
        public static String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
        
        public static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
        Date date = new Date();
        return dateFormat.format(date);
    }
  
}
