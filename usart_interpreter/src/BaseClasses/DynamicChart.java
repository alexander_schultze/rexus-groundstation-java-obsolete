package BaseClasses;


import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;

public class DynamicChart extends JPanel {
	/** Values for up to very many series in one chart. */
	private  TimeSeries series[]=new TimeSeries[12];	

        private TimeSeriesCollection dataset;
        private int maxAge;
	/**
	 * Creates a new application.
	 * @param title The Title of the New Chart
	 * @param maxAge the maximum age (in milliseconds).
	 */
	public DynamicChart(String title, int par_maxAge) {
		super(new BorderLayout());
                maxAge=par_maxAge;
		// create two series that automatically discard data more than 30
		//seconds old...

                
		dataset = new TimeSeriesCollection();

                
 

		DateAxis domain = new DateAxis("Time");
		NumberAxis range = new NumberAxis("Value");
		domain.setTickLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		range.setTickLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		domain.setLabelFont(new Font("SansSerif", Font.PLAIN, 14));
		range.setLabelFont(new Font("SansSerif", Font.PLAIN, 14));
                
		XYItemRenderer renderer = new XYLineAndShapeRenderer(true, false);
                
               
                
		renderer.setSeriesPaint(0, Color.red);
		renderer.setSeriesPaint(1, Color.green);
                renderer.setSeriesPaint(2, Color.blue);
                renderer.setSeriesPaint(3, Color.black);
                
		renderer.setStroke(new BasicStroke(3f, BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_BEVEL));
		XYPlot plot = new XYPlot(dataset, domain, range, renderer);
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		domain.setAutoRange(true);
		domain.setLowerMargin(0.0);
		domain.setUpperMargin(0.0);
		domain.setTickLabelsVisible(true);
		range.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		
                 
                
                JFreeChart chart = new JFreeChart(title,
				new Font("SansSerif", Font.BOLD, 24), plot, true);
		chart.setBackgroundPaint(Color.white);
                
                
              
		ChartPanel chartPanel = new ChartPanel(chart);
              
		chartPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createEmptyBorder(4, 4, 4, 4),
				BorderFactory.createLineBorder(Color.black)));
                
            
		add(chartPanel);
	}
	/**
	 * Adds an observation to the  time series.
	 *
	 * @param
	 */
	public void addObservation(double y, int series_nr) {
            
		series[series_nr].addOrUpdate(new Millisecond(), y);
                
               
	}
	public void addObservations(double par_value[], int nr_values) {
              for(int i=0; i<nr_values; i++){
		series[i].addOrUpdate(new Millisecond(), par_value[i]);

              }
                
              //  series[0].setNotify(true);
               // series[0].fireSeriesChanged();
              //  series[0].setNotify(false);
              
              
	}
		
         public void addSeries(String par_name[]) {
              for(int i=0; i<par_name.length; i++){
         	series[i] = new TimeSeries(par_name[i], Millisecond.class);
                if(maxAge!=0){
		series[i].setMaximumItemAge(maxAge);}
		dataset.addSeries(series[i]);
             
              }
          
             
               
                
         }
         

        
           
           
        /**
	 * Creates additional a new window for this chart
	 * @param title The Title of the New Chart
	 * @param maxAge the maximum age (in milliseconds).
	 */
	public void createChartWindowed(String par_title, int par_maxAge) {
            
            /* Create a Dataplot Extra Windows */
		JFrame frame = new JFrame("SCDI: "+par_title);
		DynamicChart panel = new DynamicChart(par_title,par_maxAge);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setBounds(200, 120, 600, 280);
		frame.setVisible(true);
		
		
		
	}
}