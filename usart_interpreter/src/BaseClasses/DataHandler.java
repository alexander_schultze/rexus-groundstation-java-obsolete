/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		22.07.2013
 *
 *
 * ------------------------------------------------------------- */


package BaseClasses;



import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.*;

/* Class: Data Handler
 * 
 * This Packages can handle incoming Data and put it into CSV and a LOG.
 * If CSV or LOG are null they will just be ignored
 * Hence it can also be used for simple Text Logging without storing Values
 */




public class DataHandler
{
  public String save_filename;
  private DynamicChart chart;
  private File file;
  private FileWriter writer;
  private int number_of_data;
  private JTextArea logtext;
  	/**
	 * Creates a new application.
	 * @param sDataName[] The Name of the Data Channels
	 * @param pChart The chart to show the data in.
         * @param sFileName The Filename where the Data will be saved (csv)
         * @param jLog The Textfield where the output will be logged to
	 */
  
  
  
    public DataHandler(String sFileName, JTextArea jLogText){
      logtext=jLogText;     
      try
	{
	 
   	     file = new File(sFileName+getDateTime()+".txt");
            writer = new FileWriter(file);
	    writer.flush();
	    
	}
	catch(IOException e)
	{
	     e.printStackTrace();
	}
    }
      
  
  public DataHandler(String sDataName[], DynamicChart pChart, String sFileName, JTextArea jLogText){
      //super();
      number_of_data=sDataName.length;
      logtext=jLogText;
      
      
      if(sFileName!=null){
      try
	{
	 
   	    file = new File(sFileName+getDateTime()+".csv");
            writer = new FileWriter(file);
            writer.append("HH:mm:ss;SysTime;LoTime;");
	    writer.append(sDataName[0]);
            writer.append(';');
	    writer.append(sDataName[1]);
            writer.append(';'); 
	    writer.append(sDataName[2]);
            writer.append(';');
 	    writer.append(sDataName[3]);
            writer.append('\n');
	    writer.flush();
	    
	}
	catch(IOException e)
	{
	     e.printStackTrace();
	}
      }
       if (chart != null){
      chart=pChart;
      chart.addSeries(sDataName);
       }
  }
  
  public void AddData(double pValues[], int nr_values){
      if(number_of_data!=nr_values){
          /* Errormessage Invalid Number of Data */
          System.out.print("Add Data: Wrong Number of Parameters");
      }
      if (chart != null){
        chart.addObservations(pValues, nr_values); };
        if (writer != null){
                try{
                    
                    
                    
                    writer.append(getTime()+";"); //SysTime
                    writer.append("0;"); //SysTime
                    writer.append("0;"); //LoTime
                     for(int i=0; i<nr_values; i++){
                    writer.append(Double.toString(pValues[i])); 
                    if (i!=nr_values-1){writer.append(';');}else{writer.append('\n');}                    
                     }
                     writer.flush();
                 }
                catch(IOException e)
                {
                     e.printStackTrace();
                }
        if (logtext != null){     
            addStringToText("["+getTime()+"]:"); //SysTime
           
            addStringToText("0|"); //SysTime
            addStringToText("0|"); //LoTime
                     for(int i=0; i<nr_values; i++){
                   addStringToText(Double.toString(pValues[i])); 
                    if (i!=nr_values-1){addStringToText("|");}else{addStringToText("\n");}  
                    
                     }
            
        }   
                
         
      }                  
      }
  
  
  
  
  
  public void addStringToFile(String sText){
      
              if (writer != null){
                try{
                   
                    writer.append(sText); 
                    writer.flush();
                 }
                catch(IOException e)
                {
                     e.printStackTrace();
                }
         
      }              
  }
  
    public void addStringToText(String sText){
     try{
      
              if (logtext != null){
                     logtext.append(sText);
                     logtext.setCaretPosition( logtext.getDocument().getLength()-1 );
         
                }  
     }catch (IllegalArgumentException ex){System.out.print(ex.getMessage());}
  }
    
        public void addString(String sText){
      
           addStringToText(sText);
           addStringToFile(sText);
  }
        
                public void addLine(String sText){
      
           addStringToText("\n"+sText);
           addStringToFile("\n"+sText);
  }
  
  
  
  
      
   public void Quit(){
       
       try{       
                     writer.flush();
                     writer.close();
                 }
                catch(IOException e)
                {
                     e.printStackTrace();
                }        
  }          
  
  
  
  
  
  public static String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
        Date date = new Date();
        return dateFormat.format(date);
    }
  
    private static String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
  
}    



   
 

  